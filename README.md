# STRAVA CUSTOM ENDPOINT
For running this api you can build docker image with given file at project root directory . 
   
     git clone PROJECT_URL 
     
     docker build -t strava_custom:latest . 
     
     docker run -p 5000:5000 -e STRAVA_API_KEY=<SUPER_SECRET_API_KEY> strava_custom:latest
     
     
 # API DOCS 
 
 If you wanna get the corrolation with leaderboards and cyclingSegments you can send request via browser endpoint from  <a href="http://localhost:5000/leaderBoard">here</a>. --LOCALHOST--
 
 On my fault I cannot find the real coordinaties of Istanbul so that , for handling this problem I added another option at leaderBoard endpoint with Queryparameters .
 
 	
    
      http://localhost:5000/leaderBoard?k1=COORDINATE1&k2=COORDINATE2&k3=COORDINATE3&k4=COORDINATE4
 
 
 <b>COORDINATE_N</b> meaning is in order by : <b>southwest corner latitutde</b>, <b>southwest corner longitude</b>, <b>northeast corner latitude</b>, <b>northeast corner longitude </b> 
 