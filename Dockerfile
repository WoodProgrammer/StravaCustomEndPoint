FROM python:latest
COPY ./api api/.
WORKDIR api
RUN pip3 install -r req.txt
#RUN  python3 -m pytest -v -s test_Service.py
#RUN  python3 -m pytest -v -s test_RestApi.py
EXPOSE 5000
ENTRYPOINT python3 api.py
