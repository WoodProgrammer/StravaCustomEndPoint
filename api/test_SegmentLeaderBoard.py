import unittest
from model import LeaderBoards

class TestSegmentLeaderBoard(unittest.TestCase):

    @unittest.skip(reason="PASSED")
    def test_leader_board(self):
        id = "5601561"
        modelObj = LeaderBoards()
        self.assertEqual(200, modelObj.getSegmentLeaderBoard(segmentId= id ))


    def test_get_segments_id(self):
        modelObj = LeaderBoards()
        data = modelObj.getSegmentLeaderBoard(segmentId = "5601561")
        print(data)
        self.assertEqual(None, data)


if __name__ == '__main__':
    unittest.main()