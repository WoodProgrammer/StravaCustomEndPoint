import requests, os

class SegmentsModel(object):


    def __init__(self):
        self.token = os.environ["STRAVA_API_KEY"]
        self.header = {'authorization': "Bearer {}".format(self.token)}


    def getCycleSegmentsAll(self, coordinates=None): ##getting all data from STRAVA API
        if coordinates != None:
            coordinates = coordinates
        else:
            coordinates = '41.00527,28.97696,42.00527,29.97696'

        cycleData = requests.get("https://www.strava.com/api/v3/segments/explore", ##sending requests to the stravaAPI
                            data=[
                     ('bounds', coordinates ),
                    ('activity_type', 'cycling'), ####Activity Type is  important for us . !
          ],
          headers={
                 "Authorization": "Bearer {}".format(self.token) ## setting authorization HEADER
            },
        )

        return cycleData.json() ##return val JSON

    @property
    def getToken(self):
        return self.token



class LeaderBoards(SegmentsModel):

    def __init__(self):
        SegmentsModel.__init__(self)


    def getSegmentLeaderBoard(self, segmentId):

        cycleData = requests.get("https://www.strava.com/api/v3/segments/{}/leaderboard".format(segmentId), ##sending requests to the stravaAPI

          headers={
                 "Authorization": "Bearer {}".format(self.token) ## setting authorization HEADER
            },
        )

        return cycleData.json()


    def getSegmentIds(self, coordinates):

        idStore = []
        segmentData = self.getCycleSegmentsAll(coordinates=coordinates) ##coordinate data to here

        for sid in segmentData["segments"]: ##setting segment ids to the id store var .
            idStore.append(sid["id"])


        return idStore







