from model import LeaderBoards
##athlete_name
class Service(LeaderBoards):
    def __init__(self):
        LeaderBoards.__init__(self)

    def generateTables(self, coordinates = None):

        ids = self.getSegmentIds(coordinates)


        tmpJson = {}

        for id in ids :
            data = self.getSegmentLeaderBoard(segmentId= id)

            for boardData in data["entries"]: ### fetching all data from json

                playerName = boardData["athlete_name"] ###more readable code .

                if playerName in tmpJson.keys():
                    tmpJson[playerName]["count"] = tmpJson[playerName]["count"] + 1
                    tmpJson[playerName]["rank"] =  tmpJson[playerName]["rank"] + boardData["rank"]

                else:
                    tmpJson[playerName] = {}
                    tmpJson[playerName]["count"] = 0
                    tmpJson[playerName]["rank"] = boardData["rank"]

        return tmpJson