from flask import Flask, jsonify, request, abort
from flask_restful import Api, Resource
from model import SegmentsModel
from service import Service

#Created by WoodProgrammer


app = Flask(__name__)
api = Api(app)
segmentModelObj = SegmentsModel()
serviceObj = Service()

class CyclingAPI(Resource):

    def get(self): ### returning cycling with spesfic coordinates or default .

        return segmentModelObj.getCycleSegmentsAll()


class SegmentLeaderBoard(Resource): ### segment leaders is ISTANBUL

    def getCoordinateString(self, requestObj): ## may be you wnat search different coordination .
        return "{},{},{},{}".format(requestObj.args["k1"],requestObj.args["k2"],requestObj.args["k3"],requestObj.args["k4"] )###fix !!!

    def get(self):
        coordinates=None

        if len(request.args) == 4:

            coordinates = self.getCoordinateString(request)

        elif len(request.args) != 0 and len(request.args) != 4: ###f parameter handling for STRAVA PROXY .
            return abort(400, "Query parameters are invalid or missing. ")

        return jsonify(serviceObj.generateTables(coordinates=coordinates))


api.add_resource(CyclingAPI, "/cyclingAll")
api.add_resource(SegmentLeaderBoard, "/leaderBoard")

if __name__ == "__main__":
    app.run(debug=True)



